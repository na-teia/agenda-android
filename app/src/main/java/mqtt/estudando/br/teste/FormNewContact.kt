package mqtt.estudando.br.teste

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.Toast
import android.view.MenuItem
import android.widget.EditText
import mqtt.estudando.br.teste.helper.ContatoHelper
import mqtt.estudando.br.teste.repository.ContatoRepository


class FormNewContact : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_studant)
    }

   override fun onCreateOptionsMenu(menu: Menu): Boolean {

        val inflater = menuInflater
        inflater.inflate(R.menu.menu_formulario, menu)
        return super.onCreateOptionsMenu(menu)
    }




    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item!!.getItemId()) {
            R.id.menu_formulario_ok -> {

                var contatoHelper = ContatoHelper(this@FormNewContact);
                val contatoVO = contatoHelper.parseFormularioContato();
                val save = ContatoRepository(this).save(contatoVO);
                Toast.makeText(this@FormNewContact, "Contato $save  cadastrado", Toast.LENGTH_SHORT).show();

                finish();
            }
            else -> {
                Toast.makeText(this@FormNewContact, "Que coisa eim!", Toast.LENGTH_SHORT).show();
            }

        }
        return super.onOptionsItemSelected(item)
    }
}
