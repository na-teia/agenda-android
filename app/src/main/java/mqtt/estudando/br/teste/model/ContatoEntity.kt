package mqtt.estudando.br.teste.model

import mqtt.estudando.br.teste.R


data class ContatoEntity(val id: Long, val name: String = "", val address: String= "") {

    constructor( name:String,address: String) : this(0,name,address)
    override fun toString(): String ="$id - $name"
}