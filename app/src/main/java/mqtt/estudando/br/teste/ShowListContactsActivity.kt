package mqtt.estudando.br.teste


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.widget.*
import mqtt.estudando.br.teste.model.ContatoEntity
import mqtt.estudando.br.teste.repository.ContatoRepository
import java.util.*
import android.view.ContextMenu
import android.view.View
import android.widget.Toast
import android.widget.AdapterView




class ShowListContactsActivity : AppCompatActivity() {

    var listViewListaAlunos: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example)

        this.showDataListView()

        val botaoNovoCadastro= findViewById(R.id.btn_novo_cadastro) as Button;

        botaoNovoCadastro.setOnClickListener {
            val intentVaiProFormulario = Intent(this@ShowListContactsActivity, FormNewContact::class.java)
            startActivity(intentVaiProFormulario)
        }
    }


    fun showDataListView(){
        val alunos = getcontatos()
        listViewListaAlunos = findViewById(R.id.lista_alunos) as ListView
        val dataAdapterList:ArrayAdapter<ContatoEntity> = ArrayAdapter<ContatoEntity>(this, android.R.layout.simple_list_item_1, alunos)
        listViewListaAlunos?.adapter= dataAdapterList;

        registerForContextMenu(listViewListaAlunos)

    }

    fun getcontatos():Array<ContatoEntity?> {
        val contatosList:ArrayList<ContatoEntity> = ContatoRepository(this).findAll();
        val contatosArray = arrayOfNulls<ContatoEntity>(contatosList.size)
        contatosList.toArray(contatosArray)
        return  contatosArray
    }


    override fun onResume() {
        super.onResume();
        this.showDataListView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenu.ContextMenuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val contatoRepository = ContatoRepository(this)

        menu!!.setHeaderTitle("O que você deseja fazer?")
        menu.add("Editar");
        menu.add("Deletar").setOnMenuItemClickListener {
                item->
                val info = menuInfo as AdapterView.AdapterContextMenuInfo
                val contato = listViewListaAlunos?.getItemAtPosition(info.position) as ContatoEntity

            val delete = contatoRepository.delete(contato);
            if(delete<0) {
                Toast.makeText(this@ShowListContactsActivity, "Erro ao deletar  Contato  ", Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(this@ShowListContactsActivity, "Contato deletado com sucesso ", Toast.LENGTH_SHORT).show();
            showDataListView()
                false
            }
    }

}
