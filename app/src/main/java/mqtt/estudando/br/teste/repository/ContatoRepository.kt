package mqtt.estudando.br.teste.repository

import android.content.Context
import mqtt.estudando.br.teste.R
import mqtt.estudando.br.teste.model.ContatoEntity
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select

class ContatoRepository(val context: Context) {


    private val tableName:String="tb_contato"

    fun findAll() : ArrayList<ContatoEntity> = context.database.use {
        val contatosList = ArrayList<ContatoEntity>()

        select(tableName , "id", "name", "address")
                .parseList(object: MapRowParser<List<ContatoEntity>> {
                    override fun parseRow(columns: Map<String, Any?>): List<ContatoEntity> {
                        val id:Long = columns.getValue("id") as Long
                        val name :String= columns.getValue("name") as String
                        val address :String = columns.getValue("address") as String
                        val contato= ContatoEntity(id, name, address)

                        contatosList.add(contato)

                        return contatosList
                    }
                })

        contatosList
    }

    fun findByName(nome:String) : ArrayList<ContatoEntity> = context.database.use {
        val contatosList = ArrayList<ContatoEntity>()
        select(tableName , "id", "name", "address").whereArgs("name like %{name}%","name" to nome)
                .parseList(object: MapRowParser<List<ContatoEntity>> {
                    override fun parseRow(columns: Map<String, Any?>): List<ContatoEntity> {
                        val id:Long = columns.getValue("id") as Long
                        val name :String= columns.getValue("name") as String
                        val address :String = columns.getValue("address") as String
                        val contato= ContatoEntity(id, name, address)

                        contatosList.add(contato)

                        return contatosList
                    }
                })

        contatosList
    }

    fun save(contato: ContatoEntity) = context.database.use {
        return@use insert(tableName,
                "name" to contato.name.toString(),
                "address" to contato.address.toString())

     }

    fun delete(note: ContatoEntity) = context.database.use {
        val delete = delete(tableName, whereClause = "id = {contactId}", args = *arrayOf("contactId" to note.id))
        delete
    }
}