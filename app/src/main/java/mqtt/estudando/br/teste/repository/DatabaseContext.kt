package mqtt.estudando.br.teste.repository

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.db.*


class DatabaseContext (ctx: Context) : ManagedSQLiteOpenHelper(ctx, DB_NAME, null, DB_VERSION), AnkoLogger {

    companion object {
        private var instance: DatabaseContext? = null
        private const val DB_NAME = "contadosDatabase"
        private const val DB_VERSION = 14
        @Synchronized
        fun getInstance(ctx: Context): DatabaseContext {
            if (instance == null) {
                instance = DatabaseContext(ctx.applicationContext)
            }
            return instance!!
        }
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int){
        db.dropTable("ContatoEntity", true)
        db.dropTable("tb_contato", true)

        db.createTable("tb_contato", true,
                "id" to INTEGER + PRIMARY_KEY +AUTOINCREMENT,
                "name" to TEXT,
                "address" to TEXT)
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable("tb_contato", true,
                "id" to INTEGER + PRIMARY_KEY +AUTOINCREMENT,
                "name" to TEXT,
                "address" to TEXT)
    }

}
val Context.database: DatabaseContext
    get() = DatabaseContext.getInstance(applicationContext)