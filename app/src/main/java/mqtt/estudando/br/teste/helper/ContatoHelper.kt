package mqtt.estudando.br.teste.helper

import android.widget.EditText
import mqtt.estudando.br.teste.FormNewContact
import mqtt.estudando.br.teste.R
import mqtt.estudando.br.teste.model.ContatoEntity


class ContatoHelper(val  activityFormNewContato: FormNewContact){

    fun parseFormularioContato():ContatoEntity{
        var nomeContato=  ( activityFormNewContato.findViewById(R.id.editTextName) as EditText ).text.toString();
        var enderecoContato=  ( activityFormNewContato.findViewById(R.id.editTextAdress) as EditText ).text.toString();
         return ContatoEntity(nomeContato, enderecoContato);
    }



}